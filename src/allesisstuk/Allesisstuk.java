/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allesisstuk;

/**
 *
 * @author l.vanlaake
 */
public class Allesisstuk {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // opgave 1
        System.out.println(tafelVanBier(2.20)[0]);
        // verwacht -> 2.20
        System.out.println(tafelVanBier(2.20)[9]);
        // verwacht -> 22.00 
        double[] tafel = tafelVanBier(2.20);
        for (int i = 0; i < tafel.length; i++) {
            System.out.println(tafel[i]);
        }
        // verwacht -> tafel van bier in de console

        // opgave 2
        String woord1 = "aap3 noot mies";
        System.out.println(maakWoord(woord1));
        // verwacht -> "aap noot mies"

        // opgave 3
        int[] shiftArray1 = {};
        shiftLeft(shiftArray1);
        // mag geen error geven
        int[] shiftArray2 = {1, 2, 3};
        System.out.println(shiftLeft(shiftArray2)[0]);
        // verwacht -> 2

        
        // opgave 4
        System.out.println(sameEnds("abXYab")); 
        // → "ab"
        System.out.println(sameEnds("xx")); 
        // → "x"
        System.out.println(sameEnds("x")); 
        // → "x"
        
        
        // opgave 5
        System.out.println(String.valueOf(fizzBuzz(1, 6)));
        // verwacht ["1", "2", "Fizz", "4", "Buzz"]
        System.out.println(String.valueOf(fizzBuzz(1, 8)));
        // verwacht ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7"]
        System.out.println(String.valueOf(fizzBuzz(1, 11)));
        // verwacht ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz"]
        System.out.println(String.valueOf(fizzBuzz(15, 17)));
        // verwacht ["FizzBuzz", "16"]
    }

//1
// Geeft een array terug van 10 bierprijzen, van 1 bier tot 10 bier
// tafelVanBier(2.0) -> {2.0, 4.0, 6.0 ...., 20.0}
    public static double[] tafelVanBier(double prijs) {
        double[] prijzen = new double[10];
        for (int i = 1; i <= prijzen.length; i++) {
            prijzen[i] = prijs * i;
            return prijzen;
        }
        return prijzen;
    }

//2
//Filtert alle cijfers en speciale karakters uit een String
    public static String maakWoord(String input) {
        char[] inputArray = input.toCharArray();
        char[] outputArray = new char[inputArray.length - 1];
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] > 'a' && inputArray[i] < 'z' || inputArray[i] > 'A' && inputArray[i] < 'Z' && outputArray[i] == ' ') {
                outputArray[i] = inputArray[i];
            }
        }
        return String.valueOf(outputArray);
    }

//3
//Return an array that is "left shifted" by one -- so {6, 2, 5, 3} returns {2, 5, 3, 6}. 
// You may modify and return the given array, or return a new array.
//
//shiftLeft([6, 2, 5, 3]) → [2, 5, 3, 6]
//shiftLeft([1, 2]) → [2, 1]
//shiftLeft([1]) → [1]
//http://codingbat.com/prob/p105031
    public static int[] shiftLeft(int[] nums) {

        int x = nums[0];
        int[] y = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            if (i == nums.length) {
                y[i] = x;
            } else {
                y[i] = nums[i + 1];
            }
        }
        return y;
    }

    //   4
//    Given a string, return the longest substring that appears at both the beginning and end of the string without overlapping. 
//    For example, sameEnds("abXab") is "ab".
//    sameEnds("abXYab") → "ab"
//    sameEnds("xx") → "x"
//    sameEnds("xxx") → "x"
    public static String sameEnds(String string) {
        char[] arr = string.toCharArray();
        int half = arr.length / 2 + 1;
        for (int i = half; i < 0; i--) {
            if (string.substring(0, i).equals(string.substring(arr.length - i))) {
                return string.substring(0, i);
            }
        }
        return "";
    }

    // 5
//  This is slightly more difficult version of the famous FizzBuzz problem which is sometimes given as a first problem for job interviews.
//  (See also: FizzBuzz Code.) Consider the series of numbers beginning at start and running up to but not including end,
//  so for example start=1 and end=5 gives the series 1, 2, 3, 4. Return a new String[] array containing the string form of these numbers,
//  except for multiples of 3, use "Fizz" instead of the number, for multiples of 5 use "Buzz", and for multiples of both 3 and 5 use "FizzBuzz".
//  In Java, String.valueOf(xxx) will make the String form of an int or other type. 
//  This version is a little more complicated than the usual version since you have to allocate and index into an array instead of just printing, 
//  and we vary the start/end instead of just always doing 1..100
//fizzBuzz(1, 6) → ["1", "2", "Fizz", "4", "Buzz"]
//fizzBuzz(1, 8) → ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7"]
//fizzBuzz(1, 11) → ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz"]
    public static String[] fizzBuzz(int start, int end) {
        String[] array = new String[end - start];
        int count = 0;
        for (int i = start; i < array.length; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                array[count] = "FizzBuzz";
                count++;
            } else if (i % 3 == 0) {
                array[count] = "Fizz";
            } else if (i % 5 == 0) {
                array[count] = "Buzz";
            } else {
                array[i] = String.valueOf(i);
            }

        }
        return array;

    }

}
